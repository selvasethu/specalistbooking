package com.example.demo.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.aspectj.apache.bcel.classfile.SimpleConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.example.demo.basemodel.Response;
import com.example.demo.exception.BaseException;
import com.example.demo.exception.SimplesConstant;



@Component
public abstract class BaseLoggerController{
	private final Logger log = LoggerFactory.getLogger(getClass());

	@ExceptionHandler(BaseException.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public Response handleException(BaseException e) {
		log.debug("BaseController handleException BaseException : "+e.getLocalizedMessage(),e);
		Response failure = failure(e.getErrorCode(),e.getErrorMessage());
		return failure;
	}
	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.OK)
	public Response handleException(Exception e) {
		log.debug("BaseController handleException Exception"+e.getLocalizedMessage(),e);
		Response failure = failure(SimplesConstant.CODE_0.getCode(),SimplesConstant.TECH_ERROR.getCode());
		return failure;
	}
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value=HttpStatus.OK)
    public Response handleException(MethodArgumentNotValidException exception) {
		FieldError fieldError = exception.getBindingResult().getFieldError();
		Response failure = failure(SimplesConstant.CODE_0.getCode(),fieldError.getDefaultMessage());
		return failure;
    }
	protected Response response() {
		return new Response();
	}
	protected Response response(String message) {
		Response response = new Response();
		response.setMessage(message);
		return response;
	}
	protected Response response(String message,Object result) {
		Response response = new Response();
		response.setMessage(message);
		response.setResult(Arrays.asList(result));
		return response;
	}
	protected Response response(Object result) {
		Response response = new Response();
		response.setResult(Arrays.asList(result));
		return response;
	}
	protected Response response(String message,List result) {
		Response response = new Response();
		response.setMessage(message);
		response.setResult(result);
		return response;
	}
	protected Response response(List result) {
		Response response = new Response();
		response.setResult(result);
		return response;
	}
	protected Response failure(String code, String message) {
		Response response = new Response();
		response.setStatus(com.example.demo.basemodel.ResponseStatus.FAILURE);
		response.setResult(new ArrayList());
		response.setResponsecode(code != null ? code : SimplesConstant.CODE_0.getCode());
		response.setMessage(message != null ? message: SimplesConstant.TECH_ERROR.getCode());
		
		return response;
	}	
	private  Object avoidNull(Object value) {
		return value==null ? "" : value;
	}
}