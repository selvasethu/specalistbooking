package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@Autowired
	private UserDetailsService jwtUserDetailsService;
	@Autowired
	private JwtRequestFilter jwtRequestFilter;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		String[] staticResources = { "/css/**", "/images/**", "/fonts/**", "/scripts/**", };

		httpSecurity.csrf().disable().authorizeRequests().antMatchers(staticResources).permitAll()		
		.antMatchers("/main/login", "/forgotpassword", "/edit/forgotpassword", "/forgot/reset","/Reset-Password",
				"/getuser","/stockid","/vendorautoid","/productpage")
		.permitAll()
		.antMatchers("/dashboard", "/dashboard_week_query", "/dashboardchart")
		.hasAnyAuthority("Company Admin", "Management", "Procurement", "R&D", "BOM Approver", "Production")
		.antMatchers("/deleteusers", "/create/users", "/datalist/users", "/edit/users")
		.hasAuthority("Company Admin")
		.antMatchers( "/deletevendor", "/datalist/vendor1","/datalist/vendorbank/vendorupload",
				"/create/vendor", "/edit/vendor", "/delete/vendorproduct","/deletevendorupload")
		.hasAnyAuthority("Company Admin", "Management","Procurement")
		.antMatchers( "/create/typemaster","/datalist/typemaster","/edit/typemaster", "/deletetypemaster")
		.hasAnyAuthority("Company Admin", "Management")
		.antMatchers("/deletecalculation", "/datalist/calculation", "/create/calculation","/edit/calculation")
		.hasAnyAuthority("Company Admin", "Management","R&D")
		.antMatchers("/deletestock", "/datalist/stock","/create/stock","/datalist/substock", "/update/stock")
		.hasAnyAuthority("Company Admin", "Management","Procurement")
		.antMatchers("/listprocessstock", "/saveprocessstock","/createschedule","/updateschedule","/details/listingerdient/calculation","/datalist/processschedule")
    	.hasAnyAuthority("Company Admin", "Management","Production","R&D", "BOM Approver")
    	.antMatchers("/deleteproduct", "/datalist/product","/create/product", "/edit/product","/create/ingredient",
    			"/edit/ingredient","/datalist/ingredient","/deleteingredient")
    	.hasAnyAuthority("Company Admin", "Management","Production")
    	.antMatchers("/editorder","/addorder","/datalist/orderrequest")
    	.hasAnyAuthority("Company Admin", "Management","Procurement","Production")
				.anyRequest().authenticated().and().
				
				exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
}