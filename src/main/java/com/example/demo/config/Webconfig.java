package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class Webconfig extends WebMvcConfigurationSupport {

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
    	
        registry
                .addResourceHandler("/css/**")
                .addResourceLocations("classpath:/static/css/");

        registry
                .addResourceHandler("/js/**")
                .addResourceLocations("classpath:/static/js/");

        registry
                .addResourceHandler("/built/**")
                .addResourceLocations("classpath:/static/built/");

        registry
                .addResourceHandler("/img/**")
                .addResourceLocations("classpath:/static/img/");

        registry
                .addResourceHandler("/fonts/**")
                .addResourceLocations("classpath:/static/fonts/");
        registry
                .addResourceHandler("/images/**")
                .addResourceLocations("classpath:/static/images/","file:DragonImage/")
                .setCachePeriod(0);

       
        registry
                .addResourceHandler("/assets/**")
                .addResourceLocations("classpath:/static/assets/");

        registry
                .addResourceHandler("/icons/**")
                .addResourceLocations("classpath:/static/icons/");
       
        

    }
    

}
